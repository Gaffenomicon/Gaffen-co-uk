const path = require('path');
const fs = require('fs');
const dynamicImports = require('dynamic-imports');
const pluginRss = require('@11ty/eleventy-plugin-rss');
const yaml = require('js-yaml');
const EleventyVitePlugin = require('@11ty/eleventy-plugin-vite');
const tsconfigPaths = require('vite-tsconfig-paths').default;
const glslify = require('vite-plugin-glslify').default;

module.exports = function (eleventyConfig) {
  let options = {
    html: true
  };
  let markdownIt = require('markdown-it')(options);
  let mila = require('markdown-it-link-attributes');

  markdownIt.use(mila, {
    pattern: /:\/\//,
    attrs: {
      target: '_blank',
      rel: 'noopener'
    }
  });
  eleventyConfig.setLibrary('md', markdownIt);
  eleventyConfig.setUseGitIgnore(false);
  eleventyConfig.addNunjucksFilter('debug', function (value) {
    console.log(value);
    return `<pre>${value ? JSON.stringify(value) : 'Variable undefined'}</pre>`;
  });
  eleventyConfig.addNunjucksShortcode('md', function (value) {
    if (value) {
      return markdownIt.render(value);
    } else {
      return '';
    }
  });
  eleventyConfig.addCollection('posts', collection => {
    let posts = collection.getFilteredByGlob('./src/content/posts/*.md');
    return posts;
  });
  eleventyConfig.addCollection('sketches', collection => {
    let sketches = collection.getFilteredByGlob('./src/content/sketches/*.md');
    return sketches;
  });
  eleventyConfig.addPlugin(pluginRss);
  eleventyConfig.addPlugin(EleventyVitePlugin, {
    viteOptions: {
      resolve: {
        alias: {
          '/assets/styles': path.resolve(__dirname, '.', 'src', 'scss'),
          '/assets/js': path.resolve(__dirname, '.', 'src', 'js'),
          '@3d': path.resolve(__dirname, '.', 'src', '3d')
        }
      },
      root: path.resolve(__dirname, '.'),
      plugins: [tsconfigPaths(), glslify()]
    }
  });
  eleventyConfig.addDataExtension('yaml', contents => yaml.load(contents));

  return {
    dir: {
      input: 'src/content',
      output: 'build',
      includes: '../../layouts',
      data: '../../data'
    },
    templateFormats: ['md', 'njk', 'js'],
    passthroughFileCopy: false
  };
};
