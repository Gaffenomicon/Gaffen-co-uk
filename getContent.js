const request = require('axios');
const { URL } = require('url');
const fs = require('fs');
const path = require('path');
require('dotenv').config();

const getContent = url => {
  // fs.readdir(directory, (err, files) => {
  //   if (err) throw err;
  //
  //   for (const file of files) {
  //     if (file != ".gitkeep") {
  //       fs.unlink(path.join(directory, file), err => {
  //         if (err) throw err;
  //       });
  //     }
  //   }
  // });

  let options = {};

  if (process.env.ACCESS_TOKEN && process.env.ACCESS_TOKEN.length > 0) {
    options = {
      params: {
        status: ['draft', 'publish']
      },
      headers: { Authorization: `Bearer ${process.env.ACCESS_TOKEN}` }
    };
  }

  request
    .get(url, options)
    .then(res => {
      const noPages = res.headers['x-wp-totalpages'];
      const pagesToFetch = new Array(noPages - 1)
        .fill(0)
        .map((el, id) => request.get(`${url}&page=${id + 2}`));
      return Promise.all([res, ...pagesToFetch]);
    })
    .then(results => Promise.all(results.map(el => el.data)))
    .then(pages => {
      return [].concat(...pages);
    })
    .then(allPosts => {
      allPosts.forEach(post => {
        const slug = post.slug.length > 0 ? post.slug : post.id;
        let tags = [];
        post._embedded['wp:term'].forEach(term => {
          if (term[0]) {
            tags.push(term[0].name);
          }
        });
        const template =
          post.template != '' ? `${post.template}.njk` : 'default.njk';
        let frontmatter = {
          layout: `posts/${template}`,
          title: post.title.rendered,
          date: post.date,
          tags: JSON.stringify(tags),
          excerpt: post.excerpt.rendered,
          templateEngineOverride: 'md',
          masto_source: post.share_on_mastodon.url
        };
        const featured_image =
          typeof post._embedded['wp:featuredmedia'] !== 'undefined'
            ? post._embedded['wp:featuredmedia'][0]
            : null;

        if (featured_image) {
          frontmatter.featured_image = JSON.stringify(featured_image);
        }

        const frontmatterKeys = Object.keys(frontmatter);
        let value = '---\n';
        value += `${frontmatterKeys
          .map(key => `${key}: ${frontmatter[key]}\n`)
          .join('')}`;
        value += '---\n';
        value += post.content.rendered;

        fs.writeFile(`./src/content/posts/${slug}.md`, value, function (err) {
          if (err) {
            return console.error(err);
          }

          console.info(`"${post.title.rendered}" was saved!`);
        });
      });
      // done();
    })
    .catch(e => {
      console.error(e);
      console.error(
        e.response.status +
          ' ' +
          e.response.statusText +
          '. Request URL: ' +
          e.response.config.url
      );
    });
};
const mainURL = process.env.BASE_URL;
const apiURL = '/content/wp/v2/posts';
const url = `${mainURL}${apiURL}?_embed&per_page=100`;

getContent(url);
