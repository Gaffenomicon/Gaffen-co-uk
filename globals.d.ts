declare module '*.vert' {
  const content: string;
  export default content;
}

declare module '*.frag' {
  const content: string;
  export default content;
}

declare module '*.glsl' {
  const content: string;
  export default content;
}

type globalUtils = {
  CreateSketchShader: (frag: string, id: string) => void;
};

declare let CreateSketchShader: (frag: string, id: string) => void;
