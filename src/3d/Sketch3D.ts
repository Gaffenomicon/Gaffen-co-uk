import World from './World';

class Sketch3D {
  canvas?: HTMLCanvasElement;
  world?: World;
  constructor(id: string) {
    const element = document.getElementById(id);
    if (element instanceof HTMLCanvasElement) {
      this.canvas = element;
      this.world = new World(element);
    }
  }
}

export default Sketch3D;
