import Sketch3D from '@3d/Sketch3D';
import * as THREE from 'three';

import vert from '@3d/shaders/ShaderExperiments.vert';

class SketchShader {
  constructor(frag: string, id: string) {
    console.log(frag, id);
    const sketch = new Sketch3D(id);
    if (sketch.world) {
      const scene = sketch.world.scene;
      const uTime = new THREE.Uniform(0);
      const plane = new THREE.PlaneGeometry();
      const shaderMaterial = new THREE.RawShaderMaterial({
        vertexShader: vert,
        fragmentShader: frag,
        uniforms: {
          uTime
        }
      });
      sketch.world.extendRenderLoop((_time, delta) => {
        uTime.value += delta * 0.75;
      });
      const mesh = new THREE.Mesh(plane, shaderMaterial);
      scene.add(mesh);
    }
  }
}

export default SketchShader;
