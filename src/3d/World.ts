import * as THREE from 'three';

type Dimensions = {
  width: number;
  height: number;
};

type ViewportInfo = {
  canvas: Dimensions & { dpr: number };
  scene: Record<string, number>;
  screen: Record<string, number>;
};

class World {
  canvas: HTMLCanvasElement;
  vp: ViewportInfo;
  renderer: THREE.WebGLRenderer;
  camera: THREE.PerspectiveCamera;
  scene: THREE.Scene;
  clock: THREE.Clock;
  disposed: boolean;
  loopCallBack?: (time: number, delta: number) => void;
  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.vp = {
      canvas: {
        width: canvas.offsetWidth,
        height: canvas.offsetHeight,
        dpr: Math.min(window.devicePixelRatio, 2)
      },
      scene: {
        width: 1,
        height: 1
      },
      screen: {
        width: window.innerWidth,
        height: window.innerHeight
      }
    };

    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      canvas,
      stencil: false
    });

    this.renderer.setSize(this.vp.canvas.width, this.vp.canvas.height, false);
    this.renderer.setPixelRatio(this.vp.canvas.dpr);

    this.camera = new THREE.PerspectiveCamera(
      45,
      this.vp.canvas.width / this.vp.canvas.height,
      0.1,
      10000
    );
    this.camera.position.set(0, 0, 5);
    this.camera.lookAt(new THREE.Vector3());
    this.scene = new THREE.Scene();
    this.clock = new THREE.Clock();
    this.scene.background = new THREE.Color('#d63939');
    this.vp.scene = this.getViewSizeAtDepth();
    this.disposed = false;
    this.renderer.setAnimationLoop(e => {
      this.loopCallBack && this?.loopCallBack(e, this.clock.getDelta());
      this.render();
    });
  }

  getViewSizeAtDepth(depth = 0) {
    const fovInRadians = (this.camera.fov * Math.PI) / 180;
    const height = Math.abs(
      (this.camera.position.z - depth) * Math.tan(fovInRadians / 2) * 2
    );
    return { width: height * this.camera.aspect, height };
  }
  dispose() {}
  addEvents() {
    window.addEventListener('resize', this.onResize);
  }
  init() {}
  extendRenderLoop(callback: (time: number, delta: number) => void) {
    this.loopCallBack = callback;
  }
  render() {
    this.renderer.render(this.scene, this.camera);
  }
  onResize = () => {
    const canvas = this.canvas;
    this.vp.canvas.width = canvas.offsetWidth;
    this.vp.canvas.height = canvas.offsetWidth;
    this.vp.canvas.dpr = Math.min(window.devicePixelRatio, 2);

    this.vp.scene.width = window.innerWidth;
    this.vp.scene.height = window.innerHeight;

    this.renderer.setSize(this.vp.canvas.width, this.vp.canvas.height, false);
    this.camera.aspect = this.vp.canvas.width / this.vp.canvas.height;
    this.camera.updateProjectionMatrix();

    this.vp.scene = this.getViewSizeAtDepth();
  };
}

export default World;
