precision highp float;

varying vec2 vUv;
uniform float uTime;

#pragma glslify: bwToRB = require(./bwToRB.glsl)

float square(vec2 uv, float size) {
  float halfSize = size * 0.5;
  float left = step(0.5 - halfSize, uv.x);
  float right = step(uv.x, 0.5 + halfSize);

  float top = smoothstep(0.5 - halfSize * 0.4, 0.5 - halfSize, uv.y);
  float bottom = smoothstep(uv.y * 0.4, uv.y, 0.5 + halfSize);

  return left * right * top * bottom;
}

void main(){
  vec3 color = vec3(0.);
  vec2 circlePos = vec2(cos(uTime), sin(uTime)) * 0.2;

  color += square(vUv, mod(0.6 + circlePos.x, 1.0));

  color += smoothstep(0.3, 0.7, vUv.x);

  color -= step(distance(vec2(0.5), vUv + circlePos), 0.5);

  float circle = smoothstep( 0.2 + circlePos.y, 0.4 + circlePos.y,  distance(vec2(0.5), vUv + circlePos));

  color += circle;

  gl_FragColor = vec4(bwToRB(color), 1.0);
}
