precision highp float;
attribute vec3 position;
attribute vec2 uv;

varying vec2 vUv;
void main(){
  vec3 transformed = position;
  transformed.xy *= 2.;
  vUv = uv;
  gl_Position = vec4(transformed, 1.);
}