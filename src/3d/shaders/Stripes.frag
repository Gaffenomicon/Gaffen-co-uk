precision highp float;

varying vec2 vUv;
uniform float uTime;

#pragma glslify: bwToRB = require(./bwToRB.glsl)

// The most basic shader
void main(){
  vec3 color = vec3(0.);
  color += vUv.xxx * 2.;
  color -= vUv.yyy * 8.;
  color += 0.5;
  color = mod(color - (uTime * 4.), 1.);
  vec3 val = step(0.5, color);
  gl_FragColor = vec4(bwToRB(val), 1.0);
}
