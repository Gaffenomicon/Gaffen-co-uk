vec3 bwToRB(vec3 val) {
  vec3 red = vec3(0.839,0.224,0.224);
  vec3 black = vec3(0.11,0.11,0.118);
  return mix(black, red, val);
}

#pragma glslify: export(bwToRB)