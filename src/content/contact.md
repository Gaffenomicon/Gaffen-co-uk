---
layout: "base.njk"
title: Contact Gaffen
---

# Contact Me

I will be uploading a CV soon. For now you can contact me at **matt [at] gaffen.co.uk** if you want to get in touch.

I can also be found:

- On twitter [@MattsDeadCat](https://twitter.com/MattsDeadCat)
- On Mastadon [@Gaffen@mastadon.social](https://mastodon.social/@gaffen)
- On Github [@Gaffen](https://github.com/Gaffen)
- On Instagram [@Gaffenomicon](https://www.instagram.com/gaffenomicon/)
