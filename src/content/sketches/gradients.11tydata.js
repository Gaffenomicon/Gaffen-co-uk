const { getShaderData } = require('../../js/utils/shaderproject');

const data = getShaderData('Gradients', 'gradients');

data.title = 'Gradients';

module.exports = data;
