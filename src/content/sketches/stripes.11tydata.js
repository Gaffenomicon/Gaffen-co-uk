const { getShaderData } = require('../../js/utils/shaderproject');

const data = getShaderData('Stripes', 'stripes');

data.title = 'Stripes';

module.exports = data;
