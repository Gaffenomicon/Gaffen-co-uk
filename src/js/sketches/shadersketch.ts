import SketchShader from '@3d/SketchShader';

(window as unknown as globalUtils).CreateSketchShader = function (
  frag: string,
  id: string
) {
  new SketchShader(frag, id);
};
