const fs = require('fs');
const path = require('path');
const glsl = require('glslify');

const getShaderData = function (shadername, sketchname) {
  return {
    shader: glsl.compile(
      fs.readFileSync(
        path.resolve(__dirname, '../../3d/shaders/' + shadername + '.frag'),
        'utf8'
      ),
      { basedir: path.resolve(__dirname, '../../3d/shaders') }
    ),
    layout: 'sketches/shader.njk',
    sketch: sketchname
  };
};

module.exports = {
  getShaderData
};
